# CRC study material

All workflows here have been developed using the [Snakemake](https://snakemake.readthedocs.io) workflow management system.
This ensures reporducibility of the analyses.
All packges and scripts used in the analyses can be found within the repective repository.


## Workflows for data processing

- Raw data processing: Submodule [crc-cleandata](https://gitlab.com/schmeierlab/crc/crc-cleandata)
- Gene expression quantification: Submodule [crc-salmon-expr-quant](https://gitlab.com/schmeierlab/crc/crc-salmon-expr-quant)
- Colorectal cancer molecular subtype classification: Submodule [crc-salmon-classify](https://gitlab.com/schmeierlab/crc/crc-salmon-classify)
- Differential gene expression analysis Stage2 cancers: Submodule [crc-salmon-dgea-stage2](https://gitlab.com/schmeierlab/crc/crc-salmon-dgea-stage2.git)




